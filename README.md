Flask Heroku Helpers
====================

 - __ReverseProxied__ (internal): Configure Flask App for Heroku reverse proxied environment
 - __Herokuify__ : Configure Flask App for Heroku environment
