import os

from flask.ext.sslify import SSLify


class ReverseProxied(object):
    """
    Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    :param app: the Flask application
    """

    def __init__(self, app):
        self.wsgi_app = app.wsgi_app
        app.wsgi_app = self

    def __call__(self, environ, start_response):
        scheme = environ.get('HTTP_X_FORWARDED_PROTO', None)
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.wsgi_app(environ, start_response)


class Herokuify(object):
    def __init__(self, app):
        preferred_url_scheme = os.getenv("PREFERRED_URL_SCHEME", "http")
        only_https = preferred_url_scheme == "https"

        app.config.update(
            # Flask config
            DEBUG=os.getenv("DEBUG", False) == "True",
            SECRET_KEY=os.getenv("SECRET_KEY", "my very simple secret key, that I need to change in production"),

            # https ?
            PREFERRED_URL_SCHEME=preferred_url_scheme,
            SESSION_COOKIE_SECURE=only_https,
        )
        if only_https:
            SSLify(app, permanent=True)

        ReverseProxied(app)
